<?php

namespace Drupal\financial\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a Loan EMI Form.
 */
class LoanEMIForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'loan_emi_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['principal'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Principal'),
      '#size' => 20,
      '#maxlength' => 150,
      '#required' => TRUE,
    ];

    $form['years'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of Years'),
      '#size' => 20,
      '#maxlength' => 5,
      '#required' => TRUE,
    ];

    $form['interest_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interest Rate Percentage'),
      '#size' => 20,
      '#maxlength' => 150,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $principal = $form_state->getValue('principal');
    $years = $form_state->getValue('years');
    $interest = $form_state->getValue('interest_rate');
    $mi = ($interest / 1200);
    $months = $years * 12;
    $amount = (($principal * $mi * pow(1 + $mi, $months))) / (pow(1 + $mi, $months) - 1);
    $amount = number_format($amount, 2, '.', '');
    $this->messenger()->addMessage($this->t("Monthly Payment = @amount", ['@amount' => $amount]));
  }

}
