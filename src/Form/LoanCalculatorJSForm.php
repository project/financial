<?php

namespace Drupal\financial\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for calculating Loan EMI interest using JS.
 */
class LoanCalculatorJSForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'loan_calculator_js_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['loan_amount_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Principle'),
      '#size' => 15,
      '#required' => TRUE,
      '#maxlength' => 64,
    ];

    $form['years_to_pay_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of Years'),
      '#size' => 15,
      '#required' => TRUE,
      '#maxlength' => 64,
    ];

    $form['loan_rate_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interest Rate Percentage'),
      '#size' => 15,
      '#required' => TRUE,
      '#maxlength' => 64,
    ];

    $form['#executes_submit_callback'] = FALSE;

    $form['calculate_2'] = [
      '#type' => 'button',
      '#value' => $this->t('Calculate'),
    ];

    $form['result_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Monthly Payment:'),
      '#size' => 15,
      '#maxlength' => 64,
      '#attributes' => ['readonly' => ['readonly']],
    ];

    $form['#attached']['library'][] = 'system/jquery';
    $form['#attached']['library'][] = 'system/drupal';
    $form['#attached']['library'][] = 'financial/financial_js';

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
