<?php

namespace Drupal\financial\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a Simple Interest Form.
 */
class SimpleInterestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_interest_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['principal'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Principal'),
      '#size' => 20,
      '#maxlength' => 150,
      '#required' => TRUE,
    ];

    $form['years'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of Years'),
      '#size' => 20,
      '#maxlength' => 5,
      '#required' => TRUE,
    ];

    $form['interest_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interest Rate Percentage'),
      '#size' => 20,
      '#maxlength' => 150,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $principal = $form_state->getValue('principal');
    $years = $form_state->getValue('years');
    $interest = $form_state->getValue('interest_rate');
    $amount = ($principal * $interest * $years) / 100;
    $amount = number_format($amount, 2, '.', '');
    $this->messenger()->addMessage($this->t("Simple Interest = @amount", ['@amount' => $amount]));

  }

}
