CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provide the useful financial calculator tools for loans, car/auto loans,
compound interest, savings, mortgages and more. Module can be useful for
Banking/Financial website and educational website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/financial

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/financial


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

 * Install this module as you would normally install a contributed
   Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

   1. Navigate to Administration > Extend and enable the module.
   2. Navigate to Administration > Structure > Block layout and
      "Place block" from "Financial Calculators" category.


MAINTAINERS
-----------

 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
